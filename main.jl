# LiMA: Linear Map Analogues for the Embedding of Pure Computations

struct Hyperparameters
    computed_value_dimensionality::Int64
end

function proc_linear_into!(linear_proc, lima_source, position_start, mv)
    position = position_start
    while position < length(lima_source)
        let
            current = lima_source[position]
            
            if is_condjump_instruction(current)
                @async proc_linear_into!(
                    linear_proc,
                    lima_source,
                    position + 1,
                    [mv ; [ (current.condition, false) ]]
                )
                @async proc_linear_into!(
                    linear_proc,
                    lima_source,
                    current.output,
                    [mv ; [ (current.condition, true) ]]
                )
            else if is_fixedjump_instruction(current)
                position = current.output
            else
                push!(
                    linear_proc,
                    (
                        if is_mov_instruction(current)
                            quote
                                $(current.output) = $(current.inputs[1])
                            end
                        else if is_xchg_instruction(current)
                            quote
                                ($(current.inputs[1]), $(current.inputs[2])) = ($(current.inputs[2]), $(current.inputs(1)))
                            end
                        else
                            quote
                                $(current.output) = $(esc(:primitive_ops)) * $(
                                    Expr(:vcat, (current.inputs)...)
                                )
                            end
                        end
                    ),
                )
            end
        end
        position += 1;
    end
end

proc_linear_into!(p, s, mv) = proc_linear_into!(p, s, 1, mv)

function codegen_all(lima_source, regs_yield, hparams) #( primitive_ops via esc at eval )
    rdim = hparams.computed_value_dimensionality
    regs_given = identify_given(lima_source)
    linear_proc = []
    proc_linear_into!(linear_proc, lima_source)
    Expr(
        :let,
        Expr(
            :block,
            (
                map(regs_given) do str
                    quote
                        $(str |> Symbol) = ones( $(rdim |> eval) )
                    end
                end
            )...,
            linear_proc...,
        ),
        map(Symbol, regs_yield),
    )
end

codegen_resolution = Dict{Int64, Expr}()

macro invoke_codegen_resolved(src_index_expr) #( primitive_ops via esc )
    codegen_resolution[src_index_expr |> eval]
end

# Prepare the resolved codegen for the LiMA algorithm
# This *must* be run before invoking any other parts of LiMA
function setup_LiMA(sources, hparams)
    for (key, (lima_source, regs_yield)) in sources
        codegen_resolution[key] = codegen_all(lima_source, regs_yield, hparams)
    end
end

# Run the LiMA algorithm
# This function *must* be differentiable with respect to primitive_ops
# ( ∂ run_LiMA(src, primitive_ops)[n] / ∂ primitive_ops[i][j,k] )
# This will probably be done with Zygote.jl, but some custom adjoints might be needed
# This is just a clearer wrapper around the actual invoke_codegen_resolved macro
function run_LiMA(src_index, primitive_ops)
    @invoke_codegen_resolved(src_index) # this uses primitive_ops via esc
end

